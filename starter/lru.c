#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

struct node{
	int frame; 
	struct node *next;
};

struct node *head = NULL; 

/* Page to evict is chosen using the accurate LRU algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int lru_evict() {

	// evict head by moving to next node
	struct node *temp = head; 
	head = head->next; 
	// return page which was evicted
	return temp->frame;
}

/* This function is called on each access to a page to update any information
 * needed by the lru algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void lru_ref(pgtbl_entry_t *p) {

	int pte_accessed = p->frame >> PAGE_SHIFT; 

	if (head == NULL){
		// this means there is no head
		// swo create new node as head node with no next value
		struct node *new_node = malloc(sizeof(struct node)); 
		new_node->frame = pte_accessed; 
		new_node->next = NULL; 
		head = new_node; 
	}

	else{ 
		// linked list exists
		struct node *temp = head; 
		while (1) { 
			//basically move pte to the end of the linked list
			if (temp->next != NULL && temp->next->frame == pte_accessed){ 
				// if hit, if skip over next node
				//as in if next node is pte being accessed skip it
				temp->next = temp->next->next;
			}
	    	if (temp->next == NULL){
	    		// if we reach end of linked list
	    		// make a insert node being accessed to the end of list
	    		struct node *end_node = malloc(sizeof(struct node)); 
	    		end_node->frame = pte_accessed; 
	    		end_node->next = NULL;
	    		temp->next = end_node;
	    		return;
	    	}
	    	// iterate through the linked list 
		    temp = temp->next;    
		}
	}
}

/* Initialize any data structures needed for this 
 * replacement algorithm 
 */
void lru_init() {
}