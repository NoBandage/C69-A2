#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

int *frames; 

// counter
int i; 

/* Page to evict is chosen using the accurate clock algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int clock_evict() {
	// make all pieces in array equal to 0
	while (frames[i] != 0){
		frames[i] = 0; 
		i++;
		if (i == memsize){
			i = 0;
		}
	}
	// return index of page to be evisted
	return i;
}

/* This function is called on each access to a page to update any information
 * needed by the clock algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void clock_ref(pgtbl_entry_t *p) {
	// set page being accessed to 1
	frames[p->frame >> PAGE_SHIFT] = 1;
}

/* Initialize any data structures needed for this 
 * replacement algorithm 
 */
void clock_init() {
	frames = malloc(memsize * sizeof(int)); 
	int i=0;
	// fill up frames array with incrementing values
	for (i = 0; i < memsize; i++){
		frames[i] = 0; 
	}
}